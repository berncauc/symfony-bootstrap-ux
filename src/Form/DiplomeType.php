<?php

namespace App\Form;

use App\Entity\Diplome;
use App\Entity\Matiere;
use App\Entity\Apprenant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DiplomeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('matieres', EntityType::class, [
                'class' => Matiere::class,
                'choice_label' => 'nom',
                'placeholder' => 'Choisir une ou plusieurs matière',
                'required' => false,
                'autocomplete' => true,
                'multiple' => true,

            ])
            ->add('apprenants', EntityType::class, [
                'class' => Apprenant::class,
                'choice_label' => 'nom',
                'placeholder' => 'Choisir un ou plusieurs apprenants',
                'required' => false,
                'autocomplete' => true,
                'multiple' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Diplome::class,
        ]);
    }
}
