<?php
namespace App\Helper;

class StringHelper
{
    public static function getCharactersBeforeChar($string = '.', $char = '.')
    {
        $dotPosition = strpos($string, $char);

        if ($dotPosition !== false) {
            return substr($string, 0, $dotPosition);
        } else {
            return $string;
        }
    }

    public static function getCharactersAfterLastChar($string = '.', $char = '.')
    {
        $dotPosition = strrpos($string, $char);

        if ($dotPosition !== false) {
            return substr($string, $dotPosition + 1);
        } else {
            return $string;
        }
    }
}