<?php

namespace App\Repository;

use App\Entity\Matiere;
use App\Repository\DiplomeRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Matiere>
 *
 * @method Matiere|null find($id, $lockMode = null, $lockVersion = null)
 * @method Matiere|null findOneBy(array $criteria, array $orderBy = null)
 * @method Matiere[]    findAll()
 * @method Matiere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatiereRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private DiplomeRepository $diplomeRepository)
    {
        parent::__construct($registry, Matiere::class);
    }

    public function save(Matiere $matiere, bool $newMatiere = false, bool $flush = false): void
    {
        if(!$newMatiere){

            // Supprimer les diplomes qui ne sont pas dans la liste des diplomes venant du formulaire
            $diplomesFromTableDiplome = $this->diplomeRepository->findByMatiereId($matiere->getId());
            $diplomeFromForm = $matiere->getDiplomes();

            foreach ($diplomesFromTableDiplome as $diplome) {

                if (!$diplomeFromForm->contains($diplome)) {
                    $diplome->removeMatiere($matiere);
                    $this->diplomeRepository->save($diplome);
                }
            }

            // Ajouter les diplomes qui sont dans la liste des diplomes venant du formulaire
            foreach ($matiere->getDiplomes() as $diplome) {

                    $diplome->addMatiere($matiere);                    
            }
        }

        $this->getEntityManager()->persist($matiere);

        if ($flush) {
            $this->getEntityManager()->flush();
        }        
    }    
    
    public function remove(Matiere $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByMatiereId($matiereId)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.diplomes', 'd')
            ->andWhere('m.id = :matiereId')
            ->setParameter('matiereId', $matiereId)
            ->getQuery()
            ->getResult();
    }





//    /**
//     * @return Matiere[] Returns an array of Matiere objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Matiere
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
