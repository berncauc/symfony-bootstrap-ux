<?php

namespace App\Repository;

use App\Entity\Diplome;
use App\Repository\ApprenantRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Diplome>
 *
 * @method Diplome|null find($id, $lockMode = null, $lockVersion = null)
 * @method Diplome|null findOneBy(array $criteria, array $orderBy = null)
 * @method Diplome[]    findAll()
 * @method Diplome[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiplomeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private ApprenantRepository $apprenantRepository)
    {
        parent::__construct($registry, Diplome::class);
    }

    /*
    Ce code est utile pour toutes les propriétés qui sont des collections d'entités
    (OneToMany, ManyToMany) 
    inversedBy: 'diplome' est le nom de la propriété dans l'entité Apprenant 
    $apprenants est constitué d'instances de Apprenant venant de la validation du formulaire
    */
    public function save(Diplome $diplome, bool $newDiplome = false, bool $flush = false): void
    {
        if(!$newDiplome){

            // Supprimer les apprenants qui ne sont pas dans la liste des apprenants venant du formulaire
            $apprenantsFromTableApprenant = $this->apprenantRepository->findByDiplomeId($diplome->getId());
            $apprenantsFromForm = $diplome->getApprenants();

            foreach ($apprenantsFromTableApprenant as $apprenant) {

                if (!$apprenantsFromForm->contains($apprenant)) {
                    $apprenant->setDiplome(null);
                    $this->apprenantRepository->save($apprenant);
                }
            }

            // Ajouter les apprenants qui sont dans la liste des apprenants venant du formulaire
            foreach ($diplome->getApprenants() as $apprenant) {

                    $apprenant->setDiplome($diplome);                    
            }
        }

        $this->getEntityManager()->persist($diplome);

        if ($flush) {
            $this->getEntityManager()->flush();
        }        
    }

    // Suppression d'un diplôme
    public function remove(Diplome $diplome, bool $flush = false): void
    {
        // Pour éviter un conflit de clé étrangère, il faut supprimer les apprenants
        // qui sont liés à ce diplôme
        $apprenants = $diplome->getApprenants();
        foreach ($apprenants as $apprenant) {
            $apprenant->setDiplome(null);
            $this->apprenantRepository->save($apprenant);
        }

        // Suppression du diplôme
        $this->getEntityManager()->remove($diplome);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    // Recherche des diplômes par matière
    public function findByMatiereId($matiereId)
    {
        return $this->createQueryBuilder('d')
            ->leftJoin('d.matieres', 'm')
            ->andWhere('m.id = :matiereId')
            ->setParameter('matiereId', $matiereId)
            ->getQuery()
            ->getResult();
    }
//    /**
//     * @return Diplome[] Returns an array of Diplome objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Diplome
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
